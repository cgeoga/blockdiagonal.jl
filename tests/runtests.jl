
using BlockDiagonal, LinearAlgebra, Test

# Block one of a two-layer BDiagonal:
A1 = randn(10, 10)
A2 = randn(10, 10)
A  = BDiagonal([A1, A2])
Af = cat(A1, A2, dims=[1,2])

# Block two of a two-layer BDiagonal:
B1 = randn(20, 20)
B2 = randn(20, 20)
B3 = randn(20, 20)
B  = BDiagonal([B1, B2, B3])
Bf = cat(B1, B2, B3, dims=[1,2])

# Block three of a two-layer BDiagonal:
C1 = randn(10, 20)
C2 = randn(20, 5)
C3 = randn(3, 4)
C  = BDiagonal([C1, C2, C3])
Cf = cat(C1, C2, C3, dims=[1,2])

# The full BDiagonal matrices and their regular array counterparts:
M1  = BDiagonal([A, B])
M1f = cat(Af, Bf, dims=[1,2])
M2  = BDiagonal([A, B, C])
M2f = cat(Af, Bf, Cf, dims=[1,2])

sample1 = randn(size(M1f, 2))
sample2 = randn(size(M2f, 2))

# Check the layered ldiv case:
@test isapprox(M1\sample1, M1f\sample1)

# Check the non-square matvec case:
@test isapprox(M2*sample2, M2f*sample2)

# Check the adjoint matvec case:
@test isapprox(sample1'*M1, sample1'*M1f)

# For the matrix-matrix operations:
M3  = BDiagonal([randn(5,5)   for _ in 1:10])
M3f = cat(M3.V..., dims=[1,2])
M4  = BDiagonal([randn(10,10) for _ in 1:5])
M4f = cat(M4.V..., dims=[1,2])

bdsolve  = M3\M4
bdmul    = M3*M4
bdsolvef = cat(bdsolve.V..., dims=[1,2])
bdmulf   = cat(bdmul.V...,   dims=[1,2])

@test isapprox(bdsolvef, M3f\M4f)
@test isapprox(bdmulf,   M3f*M4f)


