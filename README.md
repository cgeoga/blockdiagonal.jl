# BlockDiagonal.jl

A simple addition of block-diagonal matrix functionality for the Julia programming language.

For example:
```{julia}
using LinearAlgebra, BlockDiagonal
diagonal_blocks = [randn(5, 5) for _ in 1:5]
BD = BDiagonal(diagonal_blocks)
```
and then you can perform the operations that can obviously be specialized for
block-diagonal matrices, for example
```{julia}
BD * randn(size(BD, 2))
BD \ randn(size(BD, 2))
det(BD)
tr(BD)
# ... more (including in-place operations) ...
```
I have recently also added support for `ldiv!` and `\` directly between two
block matrices so long as the first argument is a refinement of the second one
with respect to block sizes. I know it would be possible to relax that
requirement, and I'll get to that at some point.
